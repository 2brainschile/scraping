#! python3
from urllib.request import urlopen
from bs4 import BeautifulSoup
import json

shoes = []
itemShow = []

html = urlopen(f"https://www.paris.cl/search?q=zapatos%20hombre")
bs = BeautifulSoup(html.read(), 'html.parser')

results = bs.find_all('div', {'class':'product-tile'}, recursive=True, limit=None)


shoes.append(itemShow) 
for item in results:
    itemShow = []
    properties = json.loads(item['data-product'])
    itemShow.append(item['data-itemid'])
    itemShow.append(properties['brand'])
    itemShow.append(properties['name'])
    itemShow.append(properties['price'])
    itemShow.append(properties['category'])
    image = item.find('a', {'class':'thumb-link'}).find('img', {'class':'img-prod'}, recursive=True)
    itemShow.append(image['data-src'])
    shoes.append(itemShow) 

print(shoes)

