import re

def isPhoneNumber(text):
    phoneNumRegex = re.compile(r'\d\d\d-\d\d\d-\d\d\d\d')
    numberFound = phoneNumRegex.findall(text)
    return numberFound

message = 'Call me at 415-555-1011 tomorrow. 415-555-9999 is my office.'
for item in isPhoneNumber(message):
    print(item)