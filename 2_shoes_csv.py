#! python3
from urllib.request import urlopen
from bs4 import BeautifulSoup
import json
import csv

shoes = []
itemShow = []

html = urlopen(f"https://www.paris.cl/search?q=zapatos%20hombre")
bs = BeautifulSoup(html.read(), 'html.parser')

results = bs.find_all('div', {'class':'product-tile'}, recursive=True, limit=None)

itemShow.append('id')
itemShow.append('brand')
itemShow.append('name')
itemShow.append('price')
itemShow.append('category')
itemShow.append('link')
shoes.append(itemShow) 
for item in results:
    itemShow = []
    properties = json.loads(item['data-product'])
    itemShow.append(item['data-itemid'])
    itemShow.append(properties['brand'])
    itemShow.append(properties['name'])
    itemShow.append(properties['price'])
    itemShow.append(properties['category'])
    image = item.find('a', {'class':'thumb-link'}).find('img', {'class':'img-prod'}, recursive=True)
    itemShow.append(image['data-src'])
    shoes.append(itemShow) 

with open("ripley_shoes.csv","w+") as ripley_csv:
    csvWriter = csv.writer(ripley_csv)
    csvWriter.writerows(shoes)

