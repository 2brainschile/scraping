# README #

Artefactos utilizando en el webinar de Web Scraping 2Brains. 

### Para qué es este repositorio ###

* Comprender las nociones básicas de web scraping con Python
* Comprender la importancia de las expresiones regulares
* Tener un ejemplo con una página de retail que pueda lanzar a cualquier persona que se interesa por esta técnica

### Cómo se configura ###

* Debes clonar el proyecto
* Instalar Python 3
* Elegir tu entorno de desarrollo Python preferido
* Ejecutarl os diferentes ejemplos y disfrutar de lo sencillo que resulta

### Descripción de los artefactos ###

* 1_shoes_capture.py -> Conexión a una URL y filtrado por etiquetas y clases HTML
* 2_shoes_csv.py -> Volcado de los resultados a un CSV
* 3_shoes_navigatepages.py  -> Estrategia de paginación para obtener todas las ocurrencias
* 4_regex_phone.py -> Ejemplo de código complejo sin expresiones regulares
* 5_regex_phone.py -> Extracción de varias ocurrencias en el mismo texto
* 6_regex_phone_regex.py -> Código mejorado usando expresiones regulares 
* 7_shoes_regex.py -> Aplicación de expresiones regulares al ejemplo de scraping
* 8_clean_ngram.py -> Utilización de n-gram para limpieza de datos

