from urllib.request import urlopen
from bs4 import BeautifulSoup
import json
import re

def getNgrams(content, n):
    content = re.sub('\n|[[\d+\]]|/', ' ', content)
    content = bytes(content, 'UTF-8')
    content = content.decode('ascii', 'ignore')
    content = content.split(' ')
    content = [word for word in content if word != '']
    output = []
    for i in range(len(content)-n+1):
        output.append(content[i:i+n])
    return output


html = urlopen(f"https://www.paris.cl/search?q=zapatos%20hombre")
bs = BeautifulSoup(html.read(), 'html.parser')

results = bs.find_all('div', {'class':'product-tile'}, recursive=True, limit=None)

shoes = []
itemShow = []
for item in results:
    itemShow = []
    properties = json.loads(item['data-product'])
    
    name = getNgrams(properties['name'], 2)
    category = getNgrams(properties['category'], 4)
    print('------------')
    print(properties['name'])
    print(name)
    print(properties['category'])
    print(category)

